#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_usart.h"
#include "misc.h"

//Deklaracja struktur do konfiguracji pinu, timer�w i kanalu timera i usarta i przerwan.
GPIO_InitTypeDef gpio;
TIM_TimeBaseInitTypeDef tim;
TIM_OCInitTypeDef channel;
USART_InitTypeDef uart;
NVIC_InitTypeDef nvic;
TIM_TimeBaseInitTypeDef tim2;
TIM_TimeBaseInitTypeDef tim3;

//jesli jest 1 to wczytaj kolejna troj-komende z kolejki
int flag = 1;

uint16_t sinetable[]  = {
  127,130,133,136,139,143,146,149,152,155,158,161,164,167,170,173,176,178,181,184,187,190,192,195,198,200,203,205,208,210,212,215,217,219,221,223,225,227,229,231,233,234,236,238,239,240,
  242,243,244,245,247,248,249,249,250,251,252,252,253,253,253,254,254,254,254,254,254,254,253,253,253,252,252,251,250,249,249,248,247,245,244,243,242,240,239,238,236,234,233,231,229,227,225,223,
  221,219,217,215,212,210,208,205,203,200,198,195,192,190,187,184,181,178,176,173,170,167,164,161,158,155,152,149,146,143,139,136,133,130,127,124,121,118,115,111,108,105,102,99,96,93,90,87,84,81,78,
  76,73,70,67,64,62,59,56,54,51,49,46,44,42,39,37,35,33,31,29,27,25,23,21,20,18,16,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,16,18,20,21,23,25,27,29,31,
  33,35,37,39,42,44,46,49,51,54,56,59,62,64,67,70,73,76,78,81,84,87,90,93,96,99,102,105,108,111,115,118,121,124
};
int intHertz = 2500;
int R = 107374182; //2500 hz
uint16_t pulse_width = 128;
uint32_t phase_accumulator = 0;
uint8_t angle = 0;

//dla drugiej fali
int intHertz2 = 2500;
int R2 = 107374182; //2500 hz
uint16_t pulse_width2 = 128;
uint32_t phase_accumulator2 = 0;
uint8_t angle2 = 0;

int playWithSecondWave = 0;

int playSound = 1;
int play = 0;

void TIM1_UP_IRQHandler()
{
    if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
    {
        send_char('k');
    }
}

//Obsluga przerwania timera.
void TIM3_IRQHandler() {
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		//zapisuje obecna szerokosc impulsu
		if (playSound && !playWithSecondWave) pwm(pulse_width);
		else if (playSound && playWithSecondWave) pwm((pulse_width + pulse_width2) / 2);
		//liczy kolejna szerokosc impulsu
		phase_accumulator += R;
		angle = (uint8_t)(phase_accumulator >> 24);
		pulse_width = sinetable[angle];

		phase_accumulator2 += R2;
		angle2 = (uint8_t)(phase_accumulator2 >> 24);
		pulse_width2 = sinetable[angle2];

		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	}
}

//dlugosc CircleBuffera (ile naraz moze byc znakow w kolejce rx i tx)
#define LENGTH 2000
//dlugosc commandChara (maksymalna dlugosc pojedynczej komendy - razem z argumentem)
#define LENGTH2 30
//ilosc wartosci pwm, pre, timer do kolejnego zagrania
#define LENGTH3 100

struct CircleBuffer {
	uint8_t buffer[LENGTH];
	int head;
	int tail;
} rx, tx; //receive do odbierania, transmit do przesylania danych

//dopisanie znaku na przod kolejki. chyba beda bledy jak to bedzie nadpisywac dane, ktore jeszcze nie
//zostaly odczytane przez pop(), ale bufor ma dlugosc 2000 wiec sie raczej nie zdazy nadpisanie
void push(uint8_t e, struct CircleBuffer* b) {
	b->buffer[b->head] = e;
	b->head++;
	if (b->head >= LENGTH) b->head = 0;
}

//pobieranie znaku od ko�ca kolejki
uint8_t pop(struct CircleBuffer* b) {
	if (b->head != b->tail){
		uint8_t returned = b->buffer[b->tail];
		b->tail++;
		if (b->tail >= LENGTH) b->tail = 0;
		return returned;
	}
	else return -1;
}

uint8_t isEmpty(struct CircleBuffer* b) {
	return b->head == b->tail;
}

int currentPwm = -1;
int currentPre = -1;
int currentTimer = -1;

struct CommandBuffer {
	int pwmBuffer[LENGTH3];
	int preBuffer[LENGTH3];
	int timerBuffer[LENGTH3];
	int head;
	int tail;
} commandBuffer;

void pushCommand(int pwmBuffer, int preBuffer, int timerBuffer) {
	commandBuffer.pwmBuffer[commandBuffer.head] = pwmBuffer;
	commandBuffer.preBuffer[commandBuffer.head] = preBuffer;
	commandBuffer.timerBuffer[commandBuffer.head] = timerBuffer;
	commandBuffer.head++;
	if (commandBuffer.head >= LENGTH3) commandBuffer.head = 0;
}

void popCommand() {
	if (commandBuffer.head != commandBuffer.tail) {
		currentPwm = commandBuffer.pwmBuffer[commandBuffer.tail];
		currentPre = commandBuffer.preBuffer[commandBuffer.tail];
		currentTimer = commandBuffer.timerBuffer[commandBuffer.tail];
		commandBuffer.tail++;
		if (commandBuffer.tail >= LENGTH3) commandBuffer.tail = 0;
	}
	else {
		currentPwm = -1;
		currentPre = -1;
		currentTimer = -1;
	}
}

//tablica znakow do przetrzymywania komend. pobierane z circle buffora rx.
char commandChar[LENGTH2];
//wskazuje w ktorym miejscu w tablicy commandChar dopisywac kolejne znaki.
int charCnt = 0;

void checkCommand() {
	//jesli bufor odbiorczy nie jest pusty.
	if (!isEmpty(&rx)) {
		//sprawdz czy najnowszy znak jest znakiem konczocym komend�, np. enter lub srednik.
		//pobierz od poczatku 3 litery i sprawdz czy kazdy z nich po kolei jest rowny p, w i m.
		//kolejne 3 litery zamien na liczbe od 0 do 999 - sprawdz czy podane cyfry sa znakami
		//w przedziale ascii tam gdzie sa cyfry, przesun ich wartosc, tak zeby wartosc char byla w
		//zakresie 0 - 9, i potem short int number = a * 100 + b * 10 + c;, i pulse PWM dla sluchawek
		//ustawiasz tyle ile jest wartosc number. Potem dorob ze naraz mozna podac wiele komend,
		//sprawdz czy dziala jak sie np. poda tylko 4 znaki i znak konczacy, dorob ze mozna podac dwa
		//argumenty i wtedy podajesz wysokosc tonu i ile czasu ma on grac, i mozna wprowadzac sekwencje
		//grajaca.
		char c = pop(&rx);
		//wysyla z powrotem chara, by bylo widac na terminalu.
		send_char(c);
		//sprawdza czy pobrany char to znak konczacy polecenie.
		if (c == 59) {
			send_char('\r');
			send_char('\n');
			//i wyzeruj wskaznik zapisu do commandString
			charCnt = 0;
			if (commandChar[0] == 'p' && commandChar[1] == 'w' && commandChar[2] == 'm' &&
				commandChar[6] == 'p' && commandChar[7] == 'r' && commandChar[8] == 'e' &&
				commandChar[14] == 't' && commandChar[15] == 'i' && commandChar[16] == 'm') {
				int pwm = (commandChar[3] - '0') * 100 + (commandChar[4] - '0') * 10 +
						   commandChar[5] - '0';
				int pre = (commandChar[9] - '0') * 10000 + (commandChar[10] - '0') * 1000 +
						  (commandChar[11] - '0') * 100 + (commandChar[12] - '0') * 10 +
						   commandChar[13] - '0';
				int timer = (commandChar[17] - '0') * 1000 + (commandChar[18] - '0') * 100 +
						    (commandChar[19] - '0') * 10 + commandChar[20] - '0';
				pushCommand(pwm, pre, timer);
				send_char('3');

			}
			//sprawdzanie rodzaju funkcji z komendy.
			//jesli trzy pierwsze litery to pwm.
			else if (commandChar[0] == 'p' && commandChar[1] == 'w' && commandChar[2] == 'm') {
				//wywoluje funkcje pwm z argumentem int, tworzonym z 5 kolejnych podanych cyfr
				pwm((commandChar[3] - '0') * 1000 + (commandChar[4] - '0') * 100 +
						(commandChar[5] - '0') * 10 + (commandChar[6] - '0'));
				send_char('p');
			}
			//steruje prescalerem. nizsze wartosci to bardziej wysokie tony, wyzsze zmieniaja staly
			//dzwiek w takie strzelanie karabinem
			else if (commandChar[0] == 'p' && commandChar[1] == 'r' && commandChar[2] == 'e') {
				pre((commandChar[3] - '0') * 10000 + (commandChar[4] - '0') * 1000 +
						(commandChar[5] - '0') * 100 + (commandChar[6] - '0') * 10 +
						commandChar[7] - '0');
			}
			else if (commandChar[0] == 'h' && commandChar[1] == 'z') {
				hz((commandChar[2] - '0') * 10000 + (commandChar[3] - '0') * 1000 +
						(commandChar[4] - '0') * 100 + (commandChar[5] - '0') * 10 +
						commandChar[6] - '0');
			}
			//wycisza jakiekolwiek dzwieki, zmieniajac pwm na 0.
			else if (commandChar[0] == 's') {
				if (playSound == 1) playSound = 0;
				else if (playSound == 0) playSound = 1;
			}
			else if (commandChar[0] == 'p' && commandChar[1] == 'l' && commandChar[2] == 'a'
					&& commandChar[3] == 'y') {
				play = 1;
			}
			else if (commandChar[0] == 'g' && commandChar[1] == 'e' && commandChar[2] == 't'
					&& commandChar[3] == 'h' && commandChar[4] == 'z') {
				getHertz();
			}
			else if (commandChar[0] == '2' && commandChar[1] == 'w' && commandChar[2] == 'a'
								&& commandChar[3] == 'v' && commandChar[4] == 'e') {
				if (playWithSecondWave == 1) {
					playWithSecondWave = 0;
					send_char('\r');
					send_char('\n');
					send_char('2');
					send_char('n');
					send_char('d');
					send_char(' ');
					send_char('w');
					send_char('a');
					send_char('v');
					send_char('e');
					send_char(' ');
					send_char('o');
					send_char('f');
					send_char('f');
					send_char('\r');
					send_char('\n');
					send_char('\r');
					send_char('\n');
				}
				else if (playWithSecondWave == 0) {
					playWithSecondWave = 1;
					send_char('\r');
					send_char('\n');
					send_char('2');
					send_char('n');
					send_char('d');
					send_char(' ');
					send_char('w');
					send_char('a');
					send_char('v');
					send_char('e');
					send_char(' ');
					send_char('o');
					send_char('n');
					send_char('\r');
					send_char('\n');
					send_char('\r');
					send_char('\n');
				}
			}
			else if (commandChar[0] == '2' && commandChar[1] == 'h' && commandChar[2] == 'z') {
							hz2((commandChar[3] - '0') * 10000 + (commandChar[4] - '0') * 1000 +
									(commandChar[5] - '0') * 100 + (commandChar[6] - '0') * 10 +
									commandChar[7] - '0');
						}
			else if (commandChar[0] == 'g' && commandChar[1] == 'e' && commandChar[2] == 't'
								&& commandChar[3] == '2' && commandChar[4] == 'h'
										&& commandChar[5] == 'z') {
							getHertz2();
						}
		}
		else if (play) {
			if (c == '1') hz(523);
			else if (c == '2') hz(587);
			else if (c == '3') hz(659);
			else if (c == '4') hz(698);
			else if (c == '5') hz(783);
			else if (c == '6') hz(880);
			else if (c == '7') hz(987);
			else if (c == '9') {
				if (playSound == 1) playSound = 0;
				else if (playSound == 0) playSound = 1;
			}
			else if (c == '0') {
				play = 0;
				send_char('\r');
				send_char('\n');
			}
		}
		//jesli to jest inny znak ASCI niz znak konczacy polecenie
		else {
			//dopisz znak do tablicy
			commandChar[charCnt] = c;
			//dodaj 1 do wskaznika
			charCnt++;
			//wyzeruj wskaznik jesli wychodzi poza zakres commandString
			if (charCnt > LENGTH2) charCnt = 0;
		}
	}
}

void pwm (int pulse) {
	channel.TIM_Pulse = pulse;
	TIM_OC1Init(TIM4, &channel);
}

void pre (int prescaler) {
	tim.TIM_Prescaler = prescaler;
	TIM_TimeBaseInit(TIM4, &tim);
}

void timer (int miliseconds) {
	tim2.TIM_Period = miliseconds;
	//TIM_TimeBaseInit(TIM3, &tim2);
	//poni�sze naprawia buga, z tym �e timer od razu ma interrupta.
	//TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	//TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
}

void hz (int hertz) {
	playSound = 1;
	R = 4294967296 * hertz / 100000;
	intHertz = hertz;
}

void hz2 (int hertz) {
	R2 = 4294967296 * hertz / 100000;
	intHertz2 = hertz;
}

void getHertz () {
	send_char('\r');
	send_char('\n');
	int hzNumber = intHertz;
	int divider = 10000;
	int nonZeroDigitSpotted = 0;
	while (divider != 0) {
		int digit = hzNumber / divider;
		if (digit != 0) nonZeroDigitSpotted = 1;
		if (digit != 0 || nonZeroDigitSpotted) send_char(digit + '0');
		hzNumber = hzNumber - divider * digit;
		if (divider == 1) divider = 0;
		else divider /= 10;
	}
	send_char(' ');
	send_char('h');
	send_char('z');
	if (!playSound) {
		send_char(' ');
		send_char('(');
		send_char('m');
		send_char('u');
		send_char('t');
		send_char('e');
		send_char('d');
		send_char(')');
	}
	send_char('\r');
	send_char('\n');
	send_char('\r');
	send_char('\n');
}

void getHertz2 () {
	send_char('\r');
	send_char('\n');
	int hzNumber = intHertz2;
	int divider = 10000;
	int nonZeroDigitSpotted = 0;
	while (divider != 0) {
		int digit = hzNumber / divider;
		if (digit != 0) nonZeroDigitSpotted = 1;
		if (digit != 0 || nonZeroDigitSpotted) send_char(digit + '0');
		hzNumber = hzNumber - divider * digit;
		if (divider == 1) divider = 0;
		else divider /= 10;
	}
	send_char(' ');
	send_char('h');
	send_char('z');
	send_char(' ');
	send_char('(');
	if (!playWithSecondWave) {
		send_char('i');
		send_char('n');
	}
	send_char('a');
	send_char('c');
	send_char('t');
	send_char('i');
	send_char('v');
	send_char('e');
	send_char(')');
	send_char('\r');
	send_char('\n');
	send_char('\r');
	send_char('\n');
}

void USART2_IRQHandler(void){
	//odbieranie znakow i dopisywanie ich do bufora rx.
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){
	    char c = USART_ReceiveData(USART2);
	    push(c, &rx);
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);

	}
	//przesylanie znakow z bufora tx
	if (USART_GetITStatus(USART2, USART_IT_TXE) != RESET){
		if (!isEmpty(&tx)){
			USART_SendData(USART2, pop(&tx));
		} else USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		USART_ClearITPendingBit(USART2, USART_IT_TXE);
	}
}

void send_char(char c) {
	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
	push(c, &tx);
	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

int main(void) {

	//Podlaczenie zegarow.
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC |
		 RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	//Konfiguracja pinu PWM.
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_6;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &gpio);

	//Wyjscie dla USARTA (linia TX)
	gpio.GPIO_Pin = GPIO_Pin_2;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &gpio);

	//Wejscie dla USARTA (linia RX)
	gpio.GPIO_Pin = GPIO_Pin_3;
	gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &gpio);

	//Ustawienie baud rate dla USARTa
	USART_StructInit(&uart);
	uart.USART_BaudRate = 9600;
	USART_Init(USART2, &uart);

	//Wlaczenie USARTa
	USART_Cmd(USART2, ENABLE);

	//Konfiguracja timera.
	TIM_TimeBaseStructInit(&tim);
	//Tryb pracy - zliczanie w gore.
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	//przy 64000 - 1 pukniecia, 6400 - 1 strza�y jak z karabinu przy 640 - 1 jest dzwiek, przy 64 - 1
	//jest dzwiek bardziej wysoki, przy niskich wartosciach mocno wysoki sie robi, az do 5 - 1, ponizej
	//tego tylko jakies chrupniecia. To jest nieaktualne, przed tym jak mialem reza i konda.
	//http://letanphuc.net/2015/06/stm32f0-timer-tutorial-and-counter-tutorial/ ctrl+f "How to calculate
	//the PWM pulse
	tim.TIM_Prescaler = 0;
	//Wywoluje przerwanie gdy doliczy od 0 do 999, co zajmuje jedna sekunde.
	tim.TIM_Period = 256;
	tim.TIM_ClockDivision = 0;
	//Zapis konfiguracji.
	TIM_TimeBaseInit(TIM4, &tim);

	//Ustawienia konfiguracji kanalu timera w trybie PWM.
	TIM_OCStructInit(&channel);
	channel.TIM_OCMode = TIM_OCMode_PWM1;
	channel.TIM_OutputState = TIM_OutputState_Enable;
	channel.TIM_Pulse = 0;
	TIM_OC1Init(TIM4, &channel);

	//Uruchomienie timera.
	TIM_Cmd(TIM4, ENABLE);

	//Kolejny timer. Do obslugi kolejkowania nut.
	TIM_TimeBaseStructInit(&tim2);
	tim2.TIM_CounterMode = TIM_CounterMode_Up;
	tim2.TIM_Prescaler = 64 - 1;
	tim2.TIM_Period = 10 - 1;
	tim2.TIM_ClockDivision = 0;
	TIM_TimeBaseInit(TIM3, &tim2);
	TIM_Cmd(TIM3, ENABLE);
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	//TIM_SetCompare1(TIM3, 10);

	//NVIC dla circle buffera.
	nvic.NVIC_IRQChannel = USART2_IRQn ;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	//Konfiguracja modulu NVIC do obslugi przerwania.
	nvic.NVIC_IRQChannel = TIM3_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	//timer i przerwanie zeby nuta przestala grac np. po 400 ms, ale to nie dziala.
	TIM_TimeBaseStructInit(&tim3);
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	tim.TIM_Prescaler = 64000 - 1;
	tim.TIM_Period = 1000 - 1;
	TIM_TimeBaseInit(TIM1, &tim3);
	TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM1, ENABLE);

	nvic.NVIC_IRQChannel = TIM1_UP_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	while (1) {
		checkCommand();
		if (flag == 1) {
			popCommand();
			if (currentPwm != -1 && currentPre != -1 && currentTimer != -1) {
				send_char('F');
				flag = 0;
				pwm(currentPwm);
				pre(currentPre);
				timer(currentTimer);
			}
		}
	}
}
